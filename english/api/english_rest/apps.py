from django.apps import AppConfig


class EnglishRestConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'english_rest'
