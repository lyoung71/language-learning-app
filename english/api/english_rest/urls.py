from django.urls import path, include
from .views import list_my_words


urlpatterns = [
    path("my_words/", list_my_words, name="list_my_words"),
]
