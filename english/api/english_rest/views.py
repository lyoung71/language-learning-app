from django.shortcuts import render
from django.views.decorators.http import require_http_methods
import json
from django.http import JsonResponse
from .models import Word
from .encoders import WordEncoder
from .acls import get_definition
# Create your views here.


@require_http_methods(["GET", "POST"])
def list_my_words(request):
    if request.method == "GET":
        words = Word.objects.all()
        return JsonResponse(
            {"words": words},
            encoder=WordEncoder,
        )
    else:
        content = json.loads(request.body)

        word = Word.objects.create(**content)
        return JsonResponse(
            word,
            encoder=WordEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def show_word_of_the_day(request, id):
    if request.method == "GET":
        try:
            word_of_the_day = Word.objects.get(id=id)
            definition = get_definition(word_of_the_day.word)
            return JsonResponse(
                {"word_of_the_day": word_of_the_day,
                    "definition": definition},
                encoder=WordEncoder,
                safe=False
            )
        except Word.DoesNotExist:
            return JsonResponse({"message": "Word does not exist"})
    elif request.method == "DELETE":
        try:
            word = Word.objects.filter(id=id).delete()
            return JsonResponse(
                word,
                encoder=WordEncoder,
                safe=False
            )
        except Word.DoesNotExist:
            return JsonResponse({"message": "Word does not exist"})
    else:
        content = json.loads(request.body)
        Word.objects.filter(id=id).update(**content)
        word = Word.objects.get(id=id)
        return JsonResponse(
            word,
            encoder=WordEncoder,
            safe=False
        )
