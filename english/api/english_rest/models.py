from django.db import models
from django.urls import reverse
# Create your models here.


class Word(models.Model):
    word = models.CharField(max_length=20)
    definition = models.CharField(max_length=100)
    example = models.CharField(max_length=100)
    already_known = models.BooleanField(default=False)

    def __str__(self):
        return self.word

    def get_api_url(self):
        return reverse("list_my_words", kwargs={"id": self.id})
