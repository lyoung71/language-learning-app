from .keys import WEBSTER_DICTIONARY_API_KEY
import json
import requests


def get_definition(word):
    url = "https://www.dictionaryapi.com/api/v3/references/collegiate/json/"
    headers = {
        "key": WEBSTER_DICTIONARY_API_KEY
    }

    payload = {
        "Headword": f"{word}",
    }

    response = requests.get(url, params=payload, headers=headers)
    dict_dict = json.loads(response.content)
    try:
        return dict_dict["def"]
    except (KeyError, IndexError):
        return None
